/* Zenmine Theme version 1.0, Copyright (C) 2019-2020 Luis Blasco www.bestredminetheme.com */


function createBtn() {
	$("input[type=submit]").addClass("btn btn-sm btn-primary");
	$(".tab-content p a.icon").addClass("btn btn-sm btn-primary");
	$("#login-submit").addClass("btn btn-sm btn-primary");
	$("input[type=submit] + a.icon").addClass("btn btn-sm btn-outline-dark");
	$(".controller-enumerations.action-index #content p > a.icon").addClass("btn btn-sm btn-primary");

	$("#content").children(".contextual").find("a:first").addClass("btn btn-sm btn-primary");
	$("#content").children(".contextual").find("a:not(:first):not(.lock)").addClass("btn btn-sm btn-outline-dark");
	
	$(".issue .description .contextual a").addClass("btn btn-sm btn-outline-dark");
	$(".issue #relations .contextual a, .issue #issue_tree .contextual a").addClass("btn btn-sm btn-outline-dark");

	$("#content .contextual a.lock").addClass("btn btn-sm btn-danger");
	$(".news.box a:last").addClass("btn btn-sm btn-outline-dark");
	$("p.buttons a").addClass("btn btn-sm btn-outline-dark");
	$(".icon-fav-off").addClass("btn btn-sm btn-outline-dark");
	$("#activity_scope_form").find("p input").addClass("btn btn-sm btn-dark");
	$("table.query-columns .buttons input[type=button]").addClass("btn btn-sm btn-outline-dark");
}

function createDOMObserver(targetNode, callback) {
	if (targetNode) {
		const config = { attributes: true, childList: true, subtree: true };
		const observer = new MutationObserver(callback);
		observer.observe(targetNode, config);	
	}
}

function init() {
	$("table").wrap("<div class='table'></div>");
	createBtn();
}


$(document).ready(function() {

	if ($('body').hasClass('action-login')){
		$('#login-form > form').prepend("<div class='logo_login'></div>");
	}

	if ($('body').hasClass('action-register')){
		
		$(window).resize(function(){
			if($('body').hasClass('action-register')){
				$('#content').css("height",$(document).height());
			} 
		});
	
		$('#content').css("height",$(document).height());

		$('#new_user').prepend("<div class='logo_login'></div>");
	}

	if ($('body').hasClass('action-lost_password')){
		$('.box.tabular').prepend("<div class='logo_login'></div>");	
	}

	$('.toggle-multiselect').click(function(){
		$(this).parent().parent().prev().addClass('multiselectarrow');
	})

		
	if ($('body').hasClass('action-login') || $('body').hasClass('action-register') || $('body').hasClass('action-lost_password')) {
		$('<div id="hero"></div>').prependTo('#content')
	} else {
		if(!$('#main').hasClass('nosidebar')){
			$('<span class="openclose"></span>').prependTo('#content');
		}
	}

	$('.projects.root .root ul.projects').click(function(e) {
	    if (e.clientX > $(this).offset().left - 21 &&
			e.clientY < $(this).offset().top + 50) {
			$('.projects.root .root ul.projects li .projects').slideToggle('fast')
	    }
	});
	

	$('.openclose').click(function(){
		$('#content').toggleClass('full-width');
		$('#sidebar').toggleClass('sidebar-hide');
	})

	$('.action-login .lost_password, .action-register .lost_password').insertAfter('#password')

	$('.action-register #new_user').wrap('<div id="login-form"></div>')

	init();


	$('td.priority').each(function() {
    	$(this).wrapInner( "<div class='priority_"+$(this).html()+"'></div>");
    })

    $('.priority.attribute .value').each(function() {
    	$(this).wrapInner( "<div class='priority_"+$(this).html()+"'></div>");
    })


	$('#filters legend').trigger('click');


	function collapsible() {

		if( $('.collapsible').parent().find('.collapsed').length >= 2 || ($('.collapsible').parent().find('fieldset').length == $('.collapsible').parent().find('.collapsed').length)){
			$('#query_form_with_buttons .buttons').css("display", "none");
			$('#query_form p.buttons').css("display", "none");
		} else {
			$('#query_form_with_buttons .buttons').css("display", "inline-block");
			$('#query_form p.buttons').css("display", "inline-block");
		}

	}

	collapsible();

	$('#query_form_content').click(collapsible);


	$('#filters legend').click(function(){
		$('#filters-table .filter .field input').click(function(){ 
			$(this).parent().parent().find('.operator').toggleClass('afterhidding')
		})
	})
	

	var number=1;

	
	$("#projects-index ul.projects > li > .root, #projects-index > ul.projects  > li  > ul.projects > li > .child").each(function() {	
		
		if($(this).next(".projects").length){
			
			$(this).prepend( "<div id='project_"+number+"' class='projectshide open'></div>" );
		
			if(localStorage.getItem("project_"+number)=="closed"){		
				$('#project_'+number).parent().parent().find('ul.projects li.child').css("display", "none");
				$('#project_'+number).removeClass("open");
				$('#project_'+number).addClass("closed");
			}
			
			number++;
		}
		
	});
	
	$('.projectshide').click(function(){
		
		var projectid = $(this).attr('id');

		if(localStorage.getItem(projectid)!="closed"){
			
			localStorage.setItem(projectid, "closed");
			$('#'+projectid).removeClass("open");
			$('#'+projectid).addClass("closed");
			$('#'+projectid).parent().parent().find('ul.projects > li.child').css("display", "none");	
			//$('#'+projectid).parent().next(".projects").css("display", "none");	
			
		} else {
			
			localStorage.setItem(projectid, "open");
			$('#'+projectid).removeClass("closed");
			$('#'+projectid).addClass("open");
			$('#'+projectid).parent().parent().find('ul.projects > li.child').css("display", "block");
			//$('#'+projectid).parent().next(".projects").css("display", "block");			
		}		
	});	

	

	$('#add_filter_select').click(function(){ 

		$('.values .toggle-multiselect').click(function(){
			$(this).parent().parent().parent().toggleClass('filteropen')
		})

		$('#filters-table .filter .field input').click(function(){ 
			$(this).parent().parent().find('.operator').toggleClass('afterhidding')
		})

	});


	$('<span class="rightarrow"></span>').insertAfter($('input[value="→"]'))
	$('<span class="leftarrow"></span>').insertAfter($('input[value="←"]'))

	$('<span class="totop"></span>').insertAfter($('input[value="⇈"]'))
	$('<span class="goup"></span>').insertAfter($('input[value="↑"]'))
	$('<span class="godown"></span>').insertAfter($('input[value="↓"]'))
	$('<span class="tobottom"></span>').insertAfter($('input[value="⇊"]'))
	

});